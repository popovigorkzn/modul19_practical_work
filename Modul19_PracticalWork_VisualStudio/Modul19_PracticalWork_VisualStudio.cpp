﻿#include <iostream>
using namespace std;

class Animal {
public:
    virtual void Voice() {
        cout << "Animal voice!";
    }
};

class Dog : public Animal {
public:
    void Voice() override {
        cout << "Woof!";
    }
};

class Cat : public Animal {
public:
    void Voice() override {
        cout << "\nMeow!\n";
    }
};

class Bird : public Animal {
public:
    void Voice() override {
        cout << "Chirp!";
    }
};

int main() {
    Animal* animals[3];
    animals[0] = new Dog();
    animals[1] = new Cat();
    animals[2] = new Bird();

    for (int i = 0; i < 3; i++) {
        animals[i]->Voice();
    }

    for (int i = 0; i < 3; i++) {
        delete animals[i];
    }

    return 0;
}